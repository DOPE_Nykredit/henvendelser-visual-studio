﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.DataVisualization.Charting;
using System.Collections;
using Henvendelser.Logic;
using Henvendelser.Logic.Statistics;

namespace Henvendelser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            LoadColumnChartData();
            Operator op = new Henvendelser.Logic.Statistics.Operator();
            DateTime start = new DateTime(2013, 06, 25, 10, 0, 0);
            Dictionary<String, List<ContactQueue>> data = op.StartCollecting(start, start);
            ContactQueue value = data["Henvendelser"][0];
        
        }
        private void LoadColumnChartData()
        {
            ContactQueue cq = new ContactQueue("Marc");
            cq.external = 50;
            ((BarSeries)Henvendelser.Series[0]).ItemsSource =
        new KeyValuePair<string, int>[]{
        new KeyValuePair<string,int>("1 ", 12),
        new KeyValuePair<string,int>("1", 25),
        new KeyValuePair<string,int>("3.", 5),
        new KeyValuePair<string,int>("4", 6),
        new KeyValuePair<string,int>("5", 10),
        new KeyValuePair<string,int>("6", 4),
        new KeyValuePair<string,int>("7", 40),
        new KeyValuePair<string,int>("8", 12),
        new KeyValuePair<string,int>("9", 25),
        new KeyValuePair<string,int>("10.", 5),
        new KeyValuePair<string,int>("11", 6),
        new KeyValuePair<string,int>("12", 10),
        new KeyValuePair<string,int>("13", 4),
        new KeyValuePair<string,int>("14", 8),
        new KeyValuePair<string,int>("15", 9),
        new KeyValuePair<string,int>(cq.type, cq.external),
        new KeyValuePair<string,int>("16", 40) };
        }

        private void btn_hentData_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Hello world");
        }



    }
}
