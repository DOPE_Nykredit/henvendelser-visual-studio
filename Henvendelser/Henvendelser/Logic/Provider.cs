﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;

namespace Henvendelser.Logic
{
    class Provider
    {
        private static Provider provide = new Provider();
        private Provider() {
            Connect();
        }
        public static Provider getInstance() {
            return provide;
        }
        private void Connect() { 
        
        }

        public List<ContactQueue> getCallback(DateTime start, DateTime end) {
            List<ContactQueue> result = new List<ContactQueue>();

            StringBuilder query = new StringBuilder();
            query.Append("SELECT LAST_UPD AS PERIOD," );
            query.Append("COUNT(CASE WHEN STATUS ='Færdig' THEN 1 END) as completed_callbacks, ");
            query.Append("COUNT(CASE WHEN SOLVED_SECONDS /60 /60 <= 2 THEN 1 END) as completed_within_2hours  ");
            query.Append("FROM KS_DRIFT.NYK_SIEBEL_CALLBACK_AGENT_H_V ");
            query.Append("WHERE  LAST_UPD  BETWEEN '" +start+ "AND "+end +" AND STATUS ='Færdig' ");
            query.Append("GROUP BY LAST_UPD");

            System.Diagnostics.Trace.Write(query);
            return result;
        }
    }
}
