﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser.Logic.Statistics.Processors
{
    class EmailProcessor : IProcessor
    {
        DateTime start;
        DateTime end;
        private Operator op;
        private Provider p;
        private List<ContactQueue> finalOutputList;
        
        public EmailProcessor(Provider p, Operator op) {
            this.op = op;
            this.p = p;
        }
        public void Start()
        {
            this.finalOutputList = new List<ContactQueue>();
            GetData();
            ProcessData();
            UpdateOperator(finalOutputList);

        }

        public void GetData()
        {

        }
        public void ProcessData()
        {

        }

        public void UpdateOperator(List<ContactQueue> list)
        {
            op.AddToList("Email",list);
        }

        public void UpdatePeriod()
        {
            this.start = op.startDate;
            this.end = op.endDate;
           
        }
    }
}
