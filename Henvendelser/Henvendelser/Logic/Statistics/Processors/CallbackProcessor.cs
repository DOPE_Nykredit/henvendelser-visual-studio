﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser.Logic.Statistics.Processors
{
    class CallbackProcessor : IProcessor
    {
        private DateTime startdate { get; set; }
        private DateTime end { get; set; }
        private List<ContactQueue> result = new List<ContactQueue>();
        private Provider p;
        private Operator o;
        public CallbackProcessor(Provider p, Operator o) 
        {
            this.o = o;
            this.p = p;
        }
        public void Start()
        {
            GetData();
            ProcessData();
            UpdateOperator(this.result);
        }

        public void GetData()
        {
            DateTime start = new DateTime (2013, 12, 12, 10, 50, 0);
            DateTime end = new DateTime(2013, 12, 15, 10, 50, 0);

            p.getCallback(start,end);
        }

        public void ProcessData()
        {

        }

        public void UpdateOperator(List<ContactQueue> list)
        {
            o.AddToList("Callback",list);
        }


        public void UpdatePeriod()
        {
            this.startdate = o.startDate;
            this.end = o.endDate;
            
        }
    }
}
