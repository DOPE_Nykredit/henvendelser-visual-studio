﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Documents;

namespace Henvendelser.Logic.Statistics
{
    class HenvendelserProcessor : IProcessor
    {
        private Provider provider;
        private List<ContactQueue> finalOutputList;
        private Operator op;
        private DateTime start;
        private DateTime end;
        public HenvendelserProcessor(Provider p, Operator o) {
            this.op = o;
            this.provider = p;
        }

        public void Start()
        {
            this.finalOutputList = new List<ContactQueue>();
            GetData();
            ProcessData();
            UpdateOperator(finalOutputList);
        }

        public void GetData()
        {

        }

        public void ProcessData()
        {
            this.finalOutputList.Add(new ContactQueue("Hello World"));
        }

        public void UpdateOperator(List<ContactQueue> list)
        {
            this.op.AddToList("Henvendelser", list);
        }


        public void UpdatePeriod()
        {
            this.start = op.startDate;
            this.end = op.endDate;
        }
    }
}
