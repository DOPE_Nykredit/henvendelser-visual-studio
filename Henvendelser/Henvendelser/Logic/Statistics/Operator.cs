﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Documents;
using Henvendelser.Logic.Statistics.Processors;

namespace Henvendelser.Logic.Statistics
{
    /// <summary>
    /// The operator class controls the collection of data from the database and maps it into a Dictoinary that is returned to the User interface
    /// </summary>
    class Operator
    {
        private Dictionary<String, List<ContactQueue>> finalList = new Dictionary<String, List<ContactQueue>>();
        private HenvendelserProcessor hp;
        private IProcessor ep;
        private Provider provider;
        private Thread tHenvendelser;
        private Thread tEmail;
        private Thread tCallback;
        private Thread[] threadArray;
        public DateTime startDate {get; set;}
        public DateTime endDate {get; set;}
        /// <summary>
        /// Operator constuctor.
        /// </summary>
        /// 
        public Operator() { 

        this.provider = Provider.getInstance();
        this.hp = new HenvendelserProcessor(this.provider, this);
        this.ep = new EmailProcessor(this.provider,this);
        // Initilizes the threads (Note that this does not start the threads)
        this.tHenvendelser = new Thread(new ThreadStart(this.hp.Start));
        this.tCallback = new Thread(new ThreadStart(this.ep.Start));
        this.threadArray = new Thread[] {tHenvendelser, tCallback, tEmail};
        }

        public Dictionary<String, List<ContactQueue>> StartCollecting(DateTime start, DateTime end)
        {
            this.startDate = start;
            this.endDate = end;
            foreach (Thread t in this.threadArray)
            {
                t.Start();
            }
            while (finalList.Count == 0) { 
            
            }
            return finalList;
        }
        public void AddToList(String key, List<ContactQueue> value){
            lock(finalList){

            this.finalList.Add(key, value);

            }
          
        }
    }
}
