﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser
{
    class ContactQueue
    {
        public int external { get; set; }
        public int internale { get; set; }
        public double totalPercentage { get; set; }
        public double answered_25_sek { get; set; }
        public int lostCalls { get; set; }
        public DateTime period { get; set; }
        public String type { get; set; }

        public ContactQueue(String type) {
            this.type = type;
        }
    }
}
