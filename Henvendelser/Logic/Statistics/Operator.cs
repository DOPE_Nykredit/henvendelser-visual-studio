﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Documents;
using Henvendelser.Logic.Statistics.Processors;

namespace Henvendelser.Logic.Statistics
{
    /// <author>Marc Rasmussen (MRCR)</author>
    /// <summary>
    /// The operator class controls the collection of data from the database,
    /// maps it into a Dictoinary that is returned to the User interface
    /// </summary>
    class Operator
    {
        private Dictionary<String, List<ContactQueue>> datalist = new Dictionary<String, List<ContactQueue>>();
        private Provider provider;
        private HenvendelserProcessor henvendelsesProcessor;
        private IProcessor emailProcessor;
        private IProcessor callbackProcessor;
        private IProcessor GeneralPercentageProcessor;
        private IProcessor telefonSvarProcentProcessor;
        private Thread tHenvendelser;
        private Thread tEmail;
        private Thread tCallback;
        private Thread tGeneralPercentage;
        private Thread tTelefonSvarProcentage;
        private Thread[] threadArray;

        /// <summary>
        /// Operator constuctor.
        /// </summary>
        public Operator()
        {
            this.provider = Provider.getInstance();
            
            // Processors
            this.henvendelsesProcessor = new HenvendelserProcessor(this.provider, this);
            this.emailProcessor = new EmailProcessor(this.provider, this);
            this.callbackProcessor = new CallbackProcessor(this.provider, this);
            this.GeneralPercentageProcessor = new GeneralPercentageProcessor(this.provider, this);
            this.telefonSvarProcentProcessor = new TelefonSvarProcentProcessor(this.provider, this);

            // Initilizes the threads (Note that this does not start the threads)
            this.tHenvendelser = new Thread(new ThreadStart(this.henvendelsesProcessor.Start));
            this.tCallback = new Thread(new ThreadStart(this.callbackProcessor.Start));
            this.tEmail = new Thread(new ThreadStart(this.emailProcessor.Start));
            this.tGeneralPercentage = new Thread(new ThreadStart(this.GeneralPercentageProcessor.Start));
            this.tTelefonSvarProcentage = new Thread(new ThreadStart(this.telefonSvarProcentProcessor.Start));
            
            //adds the threads to the array
            this.threadArray = new Thread[]
            { 
                tHenvendelser, tCallback, tEmail, tTelefonSvarProcentage, tGeneralPercentage
            };
        }

        /// <summary>
        /// This method is used to start the threads and initilize data collecting
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>

        public void Start() 
        {
            StartCollecting(this.startDate, this.endDate);
        }
        public Dictionary<String, List<ContactQueue>> StartCollecting(DateTime start, DateTime end)
        {
            resetList();
            this.startDate = start;
            this.endDate = end;
            foreach (Thread t in this.threadArray)
            {
                t.Start();
            }
            while (datalist.Count != 6)
            {

            }
            return datalist;
        }
        /// <summary>
        /// This method adds a list to the final datalist
        /// </summary>
        /// <remarks>
        /// This method is the backbone of the application.
        /// The method adds the collected and processed data to the final list.
        /// The method contains a lock to avoid execptions.
        /// </remarks>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddToList(String key, List<ContactQueue> value)
        {
            lock (datalist)
            {
                this.datalist.Add(key, value);
            }

        }
        /// <summary>
        /// resets the final list.
        /// </summary>
        private void resetList() 
        {
            this.datalist.Clear();
        }
        /*
         * start and enddate used by the processors
         */
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public Dictionary<String, List<ContactQueue>> getUpdatedDateList()
        {
            return datalist;
        }
    }
    

}
