﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Documents;

namespace Henvendelser.Logic.Statistics
{
    class HenvendelserProcessor : IProcessor
    {
        private Provider provider;
        private List<ContactQueue> finalOutputList;
        private Operator op;
        private DateTime start;
        private DateTime end;

        /// <summary>
        /// Constructor, initilize the objects
        /// </summary>
        /// <param name="p"></param>
        /// <param name="o"></param>
        public HenvendelserProcessor(Provider p, Operator o)
        {
            this.op = o;
            this.provider = p;
        }

        /// <summary>
        /// Starts the processor.
        /// </summary>
        public void Start()
        {
            this.finalOutputList = new List<ContactQueue>();
            UpdatePeriod();
            GetData();
            ProcessData();
            AddWebdesk();
            UpdateOperator(finalOutputList);
        }

        /// <summary>
        /// Gets webdesk data and adds it to the operator's list
        /// </summary>
        private void AddWebdesk()
        {
            List<ContactQueue> webdeskList = provider.GetWebdeskQueue(start ,end);
            op.AddToList("Webdesk", webdeskList);
        }

        /// <summary>
        /// Sets the list equal to the list returned by the provider
        /// </summary>
        public void GetData()
        {
            finalOutputList = provider.getHenvendelser(start, end);
        }

        public void ProcessData()
        {

        }

        /// <summary>
        /// Adds the processed list to the operator
        /// </summary>
        /// <param name="list"></param>
        public void UpdateOperator(List<ContactQueue> list)
        {
            this.op.AddToList("Henvendelser", list);
        }

        /// <summary>
        /// Updates the timeframe of which data should be collected.
        /// </summary>
        public void UpdatePeriod()
        {
            this.start = op.startDate;
            this.end = op.endDate;
        }
    }
}
