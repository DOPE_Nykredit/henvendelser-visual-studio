﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser.Logic.Statistics.Processors
{
    class TelefonSvarProcentProcessor : IProcessor
    {
        DateTime start;
        DateTime end;
        private Operator op;
        private Provider p;
        private List<ContactQueue> finalOutputList;

        public TelefonSvarProcentProcessor(Provider p, Operator o)
        {
            this.op = o;
            this.p = p;
        }
        public void Start()
        {
            UpdatePeriod();
            GetData();
            ProcessData();
            UpdateOperator(finalOutputList);
        }

        public void GetData()
        {
           finalOutputList =  p.getPhoneAnswerPercentage(start, end);
        }

        public void ProcessData()
        {

        }

        public void UpdateOperator(List<ContactQueue> list)
        {
            op.AddToList("TelefonSvarProcent",list);
        }

        public void UpdatePeriod()
        {
            this.start = op.startDate;
            this.end = op.endDate;
        }
    }
}
