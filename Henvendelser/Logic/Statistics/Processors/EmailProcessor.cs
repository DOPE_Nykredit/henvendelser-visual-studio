﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser.Logic.Statistics.Processors
{
    /// <summary>
    /// Processor to collect email data
    /// </summary>
    class EmailProcessor : IProcessor
    {
        DateTime start;
        DateTime end;
        private Operator op;
        private Provider p;
        private List<ContactQueue> finalOutputList;

        /// <summary>
        /// Constructor, initilize objects
        /// </summary>
        /// <param name="p"></param>
        /// <param name="op"></param>
        public EmailProcessor(Provider p, Operator op)
        {
            this.op = op;
            this.p = p;
        }

        /// <summary>
        /// This method starts the processor.
        /// </summary>
        public void Start()
        {

            UpdatePeriod();
            GetData();
            ProcessData();
            UpdateOperator(finalOutputList);

        }

        /// <summary>
        /// This method sets the list equal to list returned from the Provider object.
        /// </summary>
        public void GetData()
        {
            this.finalOutputList = p.getEmailData(start, end); 
        }
        public void ProcessData()
        {

        }

        /// <summary>
        /// Adds the processed list to the operator
        /// </summary>
        /// <param name="list"></param>
        public void UpdateOperator(List<ContactQueue> list)
        {
            op.AddToList("Email", list);
        }

        /// <summary>
        /// Updates the timeframe of which data should be collected.
        /// </summary>
        public void UpdatePeriod()
        {
            this.start = op.startDate;
            this.end = op.endDate;

        }
    }
}
