﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser.Logic.Statistics.Processors
{
    ///<author>Marc Rasmussen (MRCR)</author>
    /// <summary>
    /// Processor that collects Callback data.
    /// </summary>
    class CallbackProcessor : IProcessor
    {
        private DateTime startdate { get; set; }
        private DateTime end { get; set; }
        private List<ContactQueue> result = new List<ContactQueue>();
        private Provider p;
        private Operator o;

        /// <summary>
        /// Constructor for the callback processor
        /// </summary>
        /// <remarks> initilize the objects</remarks>
        /// <param name="p"></param>
        /// <param name="o"></param>
        public CallbackProcessor(Provider p, Operator o)
        {
            this.o = o;
            this.p = p;
        }

        /// <summary>
        /// Main method to start collecting
        /// </summary>
        /// <remarks>
        /// This method updates the object to fit the application standards (Start, enddate)
        /// Gets the data from the database, processes it and adds it to the datalist.
        /// </remarks>
        public void Start()
        {
            UpdatePeriod();
            GetData();
//          ProcessData();
            UpdateOperator(this.result);
        }

        /// <summary>
        /// sets the list equal to the list returned from the provider
        /// </summary>
        public void GetData()
        {
           result = p.getCallback(startdate, end);
        }

        public void ProcessData()
        {
        }

        /// <summary>
        /// adds the datalist to the operator's list
        /// </summary>
        /// <param name="list"></param>
        public void UpdateOperator(List<ContactQueue> list)
        {
            o.AddToList("Callback", list);
        }

        /// <summary>
        /// Updates the period of which data should be collected.
        /// </summary>
        public void UpdatePeriod()
        {
            this.startdate = o.startDate;
            this.end = o.endDate;
        }
    }
}
