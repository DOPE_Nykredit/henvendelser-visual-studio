﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

namespace Henvendelser.Logic.Statistics
{
    /// <summary>
    /// This interface is the backbone of the processors
    /// </summary>
    interface IProcessor
    {

        void Start();
        void GetData();
        void ProcessData();
        void UpdateOperator(List<ContactQueue> list);
        void UpdatePeriod();
    }
}
