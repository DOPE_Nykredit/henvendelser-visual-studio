﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Henvendelser.Objects;

namespace Henvendelser.Logic.Statistics.Processors
{
    class GeneralPercentageProcessor : IProcessor
    {
        private List<ContactQueue> finalDataList = new List<ContactQueue>();
        private Provider p;
        private Operator op;
        private DateTime start;
        private DateTime end;

        /// <summary>
        /// Constructor, init the objects
        /// </summary>
        /// <param name="p"></param>
        /// <param name="op"></param>
        public GeneralPercentageProcessor(Provider p, Operator op) 
        {
            this.op = op;
            this.p = p;
        
        }
        
        /// <summary>
        /// Starts the processor.
        /// </summary>
        public void Start()
        {
            UpdatePeriod();
            GetData();
            ProcessData();
            UpdateOperator(finalDataList);
        }

        /// <summary>
        /// Sets the list equal to the list returned by the provider
        /// </summary>
        public void GetData()
        {
          this.finalDataList = p.GetGeneralPhonePercentageData(start, end);

        }

        /// <summary>
        /// This method processes the collected datalist
        /// </summary>
        /// <remarks>
        /// gets an additonal dataset and adds the email percentage data to the current list.
        /// </remarks>
        public void ProcessData()
        {
            foreach (ContactQueue contactqueue in p.GetGeneralEmailPercentageData(start, end))
            {
                foreach (ContactQueue emailDataQueue in finalDataList)
                {
                    if (contactqueue.period == emailDataQueue.period)
                    {
                        ((GeneralPercentageQueue)contactqueue).mail_answer_percentage = ((GeneralPercentageQueue)emailDataQueue).mail_answer_percentage;
                    }
                }
            }

        }

        /// <summary>
        /// Adds the processed list to the operator
        /// </summary>
        /// <param name="list"></param>
        public void UpdateOperator(List<ContactQueue> list)
        {
            op.AddToList("Percentage", list);
        }

        /// <summary>
        /// Updates the timeframe of which data should be collected.
        /// </summary>
        public void UpdatePeriod()
        {
            this.start = op.startDate;
            this.end = op.endDate;
        }
    }
}
