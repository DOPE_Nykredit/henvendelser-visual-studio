﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DOPeDatabaseProvider.Connection;
using DOPeDatabaseProvider.DataSupport;
using DOPeDatabaseProvider.SQL;
using Henvendelser.Objects;
namespace Henvendelser.Logic
{
    /// <author>Marc Rasmussen (MRCR)</author>
    /// <summary>
    /// This class controls input output stream from the application to the database
    /// </summary>
    /// <remarks>
    /// The goal of this class is the obtain data for the application to use.
    /// All SQL statements have been build so that each statement also processes the data.
    /// When data is collected the data is thrown into objects and in the end added to the result list.
    /// The result list is then returned for the application to process into charts
    /// 
    /// - NOTE
    /// Any addition to this class must be done in the same way as existing methods.
    /// All data processing should (if possible) happen at the database and NOT by the application.
    /// </remarks>
    class Provider
    {
        private static Provider provide = new Provider();
        private KSDriftConnection con;
        private Provider()
        {
            Connect();
        }
        public static Provider getInstance()
        {
            return provide;
        }
        private void Connect()
        {
            this.con = new KSDriftConnection();
        }

        /// <summary>
        /// This method collects callback data
        /// </summary>
        /// <remarks>
        /// The following data is collected:
        /// -Completed callbacks
        /// - Completed within two hours
        /// - Percentage
        /// </remarks>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>List</returns>
        public List<ContactQueue> getCallback(DateTime startDate, DateTime endDate)
        {
            List<ContactQueue> contactQueues = new List<ContactQueue>();
            SqlQuery query = new SqlQuery(con,
                                          " SELECT   TRUNC(LAST_UPD) AS PERIOD, "+
		                                  "COUNT(CASE WHEN SOLVED_SECONDS /60 /60 <= 2 THEN 1 END) as completed_within_2hours, "+
                                          "COUNT(CASE WHEN STATUS ='Færdig' THEN +1 END) as completed_callbacks, "+
                                          "ROUND(COUNT(CASE WHEN SOLVED_SECONDS /60 /60 <= 2 THEN 1 END) / COUNT(CASE WHEN STATUS ='Færdig' THEN 1 END) * 100) as Percentage "+
                                          "FROM KS_DRIFT.NYK_SIEBEL_CALLBACK_AGENT_H_V "+
                                          "WHERE LAST_UPD BETWEEN "+ OracleConversion.ToOracleDate(startDate) +" AND "+ OracleConversion.ToOracleDate(endDate)+ " "+
                                          "AND STATUS ='Færdig' "+
                                          "AND AGENTGROUP IN ('Hovednumre','Forsikring','Hotline','Kunder') "+
                                          "GROUP BY TRUNC(LAST_UPD)");

            query.Execute();
            SqlQueryResults results = query.Results;
            while (results.Read())
            {
                DateTime period = (DateTime)results.GetDateTime("PERIOD");
                Callback cb = new Callback("Callback", period);

                cb.completedCallbacks = (int)results.GetInteger("completed_callbacks");
                cb.completed_within_timeframe = (int)results.GetInteger("completed_within_2hours");
                cb.answerPercentage = (int) results.GetInteger("Percentage");
                contactQueues.Add(cb);
               
                Console.WriteLine(cb.completedCallbacks);

            }

            System.Diagnostics.Trace.Write(query);
            return contactQueues;
        }
        /// <summary>
        /// This method collects data containing phone percentage
        /// </summary>
        /// <remarks>
        /// The following data is collected:
        /// Queue name 
        /// Percentage
        /// </remarks>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>List</returns>
        public List<ContactQueue> getPhoneAnswerPercentage(DateTime startDate, DateTime endDate)
        {
            List<ContactQueue> contactQueues = new List<ContactQueue>();
            SqlQuery query = new SqlQuery(con,
                                          "SELECT   TRUNC(TIDSPUNKT) AS PERIOD, "+
                                          "(case when QUEUE in ('Erhverv', 'ErhvervOverflow') then 'Erhverv' "+
                                          "else QUEUE"+
                                          " end) as QUEUE, "+
		                                  "ROUND(SUM(CASE WHEN BESVARET_25_SEK > 0 THEN BESVARET_25_SEK END ) / SUM(CASE WHEN ANTAL_KALD > 0 THEN ANTAL_KALD END) * 100) AS SVAR_PROCENT "+
                                          "FROM KS_DRIFT.PERO_NKM_KØ_OVERSIGT "+
                                          "WHERE TIDSPUNKT >= " + OracleConversion.ToOracleDate(startDate) + " " +
                                          "AND TIDSPUNKT <= " + OracleConversion.ToOracleDate(endDate) + " " +
                                          "AND ANTAL_KALD > 0 "+
                                          "AND QUEUE not in ('TekniskHotline') "+
                                          "GROUP BY TRUNC(TIDSPUNKT), " +
                                          "(case when QUEUE in ('Erhverv', 'ErhvervOverflow') then 'Erhverv' "+
                                          "else QUEUE "+
                                          "end) "+
                                          "ORDER BY PERIOD DESC");
            Console.WriteLine("SELECT   TRUNC(TIDSPUNKT) AS PERIOD, " +
                                          "(case when QUEUE in ('Erhverv', 'ErhvervOverflow') then 'Erhverv' " +
                                          "else QUEUE" +
                                          " end) as QUEUE, " +
                                          "ROUND(SUM(CASE WHEN BESVARET_25_SEK > 0 THEN BESVARET_25_SEK END ) / SUM(CASE WHEN ANTAL_KALD > 0 THEN ANTAL_KALD END) * 100) AS SVAR_PROCENT " +
                                          "FROM KS_DRIFT.PERO_NKM_KØ_OVERSIGT " +
                                          "WHERE TIDSPUNKT >= " + OracleConversion.ToOracleDate(startDate) + " " +
                                          "AND TIDSPUNKT <= " + OracleConversion.ToOracleDate(endDate) + " " +
                                          "AND ANTAL_KALD > 0 " +
                                          "AND QUEUE not in ('TekniskHotline') " +
                                          "GROUP BY TRUNC(TIDSPUNKT), " +
                                          "(case when QUEUE in ('Erhverv', 'ErhvervOverflow') then 'Erhverv' " +
                                          "else QUEUE " +
                                          "end) " +
                                          "ORDER BY PERIOD DESC");
            query.Execute();
            SqlQueryResults results = query.Results;
            while (results.Read())
            {
                PhonePercentQueue ppq = new PhonePercentQueue(results.GetString("QUEUE"), (DateTime)results.GetDateTime("PERIOD"));
                ppq.answerPercentage =   (results.GetInteger("SVAR_PROCENT") == null ) ? 0 : (int) results.GetInteger("SVAR_PROCENT"); 

                contactQueues.Add(ppq);
            }

            return contactQueues;
        }
        /// <summary>
        /// This method collects email data
        /// </summary>
        /// <remarks>
        /// The following data is collected:
        /// -E-mail without campain
        /// -E-mail Total (with campain)
        /// -Campain Total
        /// -E-mail percentage (ms)
        /// -E-mail percentage total
        /// </remarks>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>List</returns>
        public List<ContactQueue> getEmailData(DateTime start, DateTime end) 
        {
            List<ContactQueue> contactqueues = new List<ContactQueue>();
            SqlQuery query = new SqlQuery(con,
                                          "SELECT   TRUNC(AA.DATO) AS PERIOD, " +
                                          "SUM(B9_IND + BEH_IND) AS mail_without_campain, " +
                                          "SUM(KAMP_IND) AS KAMPANGE_IALT, " +
                                          "ROUND(SUM((B9_IND+BEH_IND - (BEH_FEJL + B9_FEJL)) / (B9_IND+BEH_IND) * 100)) AS mail_svarPercent_ms, " +
                                          "ROUND(SUM((KS_IND - (BEH_FEJL + B9_FEJL + KAMP_FEJL)) / (KS_IND) * 100)) AS mail_svarPercent_ialt " +
                                          "FROM KS_DRIFT.DATO AA " +
                                          "LEFT JOIN KS_DRIFT.EMAIL_STAT BB ON AA.DATO = BB.DATO " +
                                          "WHERE AA.DATO >= " + OracleConversion.ToOracleDate(start) + " "+
                                          "AND AA.DATO <= " + OracleConversion.ToOracleDate(end) + " "+
                                          "GROUP BY TRUNC(AA.DATO) "+
                                          "ORDER BY PERIOD DESC");

            query.Execute();
            SqlQueryResults results = query.Results;
            while (results.Read())
            {
                EmailQueue eq = new EmailQueue((DateTime)results.GetDateTime("PERIOD"));
                eq.mail_without_campain = (int)results.GetInteger("mail_without_campain");
                eq.campain_mail = (int)results.GetInteger("KAMPANGE_IALT");
                eq.mail_percentage_ms = (int)results.GetInteger("mail_svarPercent_ms");
                eq.mail_percentage_total = (int)results.GetInteger("mail_svarPercent_ialt");

                contactqueues.Add(eq);
            }
            return contactqueues;
        
        }
        /// <summary>
        /// This method collects data containing internal and external call 
        /// </summary>
        /// <remarks>
        /// The following data is collected:
        /// - External calls
        /// - Internal calls
        /// </remarks>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>List</returns>
        public List<ContactQueue> getHenvendelser(DateTime start, DateTime end)
        {
            List<ContactQueue> contactqueues = new List<ContactQueue>();
             SqlQuery query = new SqlQuery(con,
                                           "SELECT   TRUNC(TIDSPUNKT) AS PERIOD, "+
                                           "(case "+
                                           "when QUEUE in ('Erhverv', 'ErhvervOverflow') then 'Erhverv' "+
                                           "when QUEUE in ('Hotline', 'TekniskHotline') then 'Hotline+TekniskHotline' "+
                                           "else QUEUE end "+
                                           " ) as QUEUE, "+
                                           "SUM(ANTAL_KALD) AS CALLS, "+
                                           "SUM(INTERN_KALD) AS INTERNAL_CALLS "+
                                           "FROM     KS_DRIFT.PERO_NKM_KØ_OVERSIGT "+
                                           "WHERE    TIDSPUNKT >= "+OracleConversion.ToOracleDate(start)+" "+
                                           "AND      TIDSPUNKT <= "+OracleConversion.ToOracleDate(end)+" "+
                                           "GROUP BY TRUNC(TIDSPUNKT), QUEUE "+
                                           "ORDER BY PERIOD DESC");
             query.Execute();
             SqlQueryResults results = query.Results;
             while (results.Read())
             {
                 HenvendelserQueue hq = new HenvendelserQueue(results.GetString("QUEUE"),(DateTime)results.GetDateTime("PERIOD"));
                 hq.externalCalls = (int)results.GetInteger("CALLS");
                 hq.internalCalls = (int)results.GetInteger("INTERNAL_CALLS");

                 contactqueues.Add(hq);
             }

            return contactqueues;
        }

        /// <summary>
        /// This method collects webdesk data
        /// </summary>
        /// <remarks>
        /// The following data is collected:
        /// - Total webdesk count
        /// - Answer within Nykredit's service goal
        /// - Answer percentage total
        /// </remarks>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>List</returns>
        public List<ContactQueue> GetWebdeskQueue(DateTime start, DateTime end)
        {
            List<ContactQueue> contactqueues = new List<ContactQueue>();
            SqlQuery query = new SqlQuery(con,
                                          "SELECT TRUNC(DATETIME) AS PERIOD, " +
                                          "COUNT(CASE WHEN INITIALS is not null AND RESPONSE_TIME BETWEEN 0 AND 30 THEN 1 END) AS ANSWERED_WITHIN_30_SEC, " +
                                          "COUNT(*) AS TOTAL_COUNT, " +
                                          "ROUND(COUNT(CASE WHEN INITIALS is not null AND RESPONSE_TIME BETWEEN 0 AND 30 THEN 1 END) / COUNT(*) * (100)) as ANSWER_PERCENTAGE " +
                                          "FROM KS_DRIFT.V_WEBDESK_SERVICECENTER " +
                                          "WHERE    DATETIME BETWEEN "+ OracleConversion.ToOracleDate(start)+" AND "+OracleConversion.ToOracleDate(end)+ " "+
                                          "GROUP BY TRUNC(DATETIME) " +
                                          "ORDER BY PERIOD DESC");
            query.Execute();
            SqlQueryResults results = query.Results;
            while (results.Read())
            {
                WebdeskQueue wq = new WebdeskQueue((DateTime)results.GetDateTime("PERIOD"));
                wq.total_amount = (int)results.GetInteger("TOTAL_COUNT");
                wq.answerPercentage_8030 = (int)results.GetInteger("ANSWERED_WITHIN_30_SEC");
                wq.answerPercentage_Total = (int)results.GetInteger("ANSWER_PERCENTAGE");

                contactqueues.Add(wq);
            }
            return contactqueues;
        }
          
        /// <summary>
        /// This method collects general percentage data
        /// </summary>
        /// <remarks>
        /// The following data is collected:
        /// - Phone Totat & Nykredit's service goal 80/25
        /// - Webdesk Total & Nykredit's service goal 80/30
        /// </remarks>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>List</returns>
        public List<ContactQueue> GetGeneralPhonePercentageData(DateTime start, DateTime end)
        {
            List<ContactQueue> contactQueues = new List<ContactQueue>();
            SqlQuery query = new SqlQuery(con,
                                         "SELECT  DISTINCT TRUNC(TIDSPUNKT) AS PERIOD, " +
                                         "ROUND(SUM(phone.ANTAL_BESVARET) / SUM(phone.ANTAL_KALD) * 100) AS PHONE_ANSWER_TOTAL, " +
                                         "ROUND(SUM(CASE WHEN BESVARET_25_SEK > 0 THEN BESVARET_25_SEK END ) / SUM(CASE WHEN ANTAL_KALD > 0 THEN ANTAL_KALD END) * 100) AS PHONE_ANSWER_PERCENTAGE_8025, " +
                                         "ROUND(COUNT(CASE WHEN web.INITIALS is not null AND web.RESPONSE_TIME BETWEEN 0 AND 30 THEN 1 END) / COUNT(web.CALL_ID) * (100)) as WEB_ANSWER_PERCENTAGE_8030, " +
                                         "ROUND(COUNT(CASE WHEN web.INITIALS is not null THEN 1 END) / COUNT(web.CALL_ID) *100) AS WEB_ANSWER_PERCENTAGE_TOTAL " +
                                         "FROM     KS_DRIFT.PERO_NKM_KØ_OVERSIGT phone INNER JOIN KS_DRIFT.V_WEBDESK_SERVICECENTER web ON phone.TIDSPUNKT = TRUNC(web.DATETIME) " +
                                         "WHERE    TIDSPUNKT >= "+OracleConversion.ToOracleDate(start)+ " "+
                                         "AND      TIDSPUNKT <= "+OracleConversion.ToOracleDate(end)+" "+
                                         "GROUP BY TRUNC(TIDSPUNKT) ORDER BY PERIOD DESC");

            query.Execute();
            SqlQueryResults results = query.Results;

            while (results.Read())
            {

                GeneralPercentageQueue gpq = new GeneralPercentageQueue((DateTime)results.GetDateTime("PERIOD"));
                gpq.phone_answer_total = (int)results.GetInteger("PHONE_ANSWER_TOTAL");
                gpq.phone_answer_percentage_8025 = (int)results.GetInteger("PHONE_ANSWER_PERCENTAGE_8025");
                gpq.web_answer_percentage_8030 = (int)results.GetInteger("WEB_ANSWER_PERCENTAGE_8030");
                gpq.web_answer_percentage_total = (int)results.GetInteger("WEB_ANSWER_PERCENTAGE_TOTAL");

                contactQueues.Add(gpq);
            }
            return contactQueues;
        }

        /// <summary>
        /// This method collects the general email percentage statistics.
        /// </summary>
        /// <remarks>
        /// The following data is collected:
        /// - Mail: Total & within 2 hours 
        /// </remarks>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>List</returns>
        public List<ContactQueue> GetGeneralEmailPercentageData(DateTime start, DateTime end)
        {
            List<ContactQueue> contactQueues = new List<ContactQueue>();
            SqlQuery query = new SqlQuery(con,
                                          "SELECT TRUNC(AA.DATO) AS PERIOD, " +
                                          "ROUND(SUM((B9_IND+BEH_IND - (BEH_FEJL + B9_FEJL)) / (B9_IND+BEH_IND) * 100)) AS mail_svarPercent_ms " +
                                          "FROM KS_DRIFT.DATO AA " +
                                          "LEFT JOIN KS_DRIFT.EMAIL_STAT BB ON AA.DATO = BB.DATO " +
                                          "WHERE AA.DATO >= "+OracleConversion.ToOracleDate(start) +" "+
                                          "AND AA.DATO <= "+OracleConversion.ToOracleDate(end)+" "+
                                          "GROUP BY TRUNC(AA.DATO) " +
                                          "ORDER BY TRUNC(AA.DATO) DESC ");
            query.Execute();
            SqlQueryResults results = query.Results;
            while(results.Read())
            {
                GeneralPercentageQueue gpq = new GeneralPercentageQueue((DateTime)results.GetDateTime("PERIOD"));
                gpq.mail_answer_percentage = (int)results.GetInteger("mail_svarPercent_ms");
           
                contactQueues.Add(gpq);
            }
            return contactQueues;
        }
    }    
        
}
