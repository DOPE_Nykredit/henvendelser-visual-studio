﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser.Objects
{
    class HenvendelserQueue : ContactQueue
    {
        public HenvendelserQueue(String type, DateTime period) 
        {
            this.type = type;
            this.period = period;
        }
        public int internalCalls {get; set;}
        public int externalCalls { get; set; }
        public override DateTime period { get; set; }
        public override string type { get; set; }
        public override string toString()
        {
            return type;
        }
        public Boolean isInternal()
        {
            return type != "NykreditKunder" || type != "Webdesk" || type =="BasisIntern";
        }
    }
}
