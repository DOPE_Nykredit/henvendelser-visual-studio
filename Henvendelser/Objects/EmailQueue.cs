﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser.Objects
{
    class EmailQueue : ContactQueue
    {

        public int mail_without_campain { get; set; }
        public int campain_mail { get; set; }
        public int mail_percentage_ms { get; set; }
        public int mail_percentage_total { get; set; }
        public EmailQueue(DateTime period)
        {
            this.period = period;
        }

        public override DateTime period { get; set; }
        public override string type{ get; set; }
        public override string toString()
        {
            return period.ToString();
        }
    }
}
