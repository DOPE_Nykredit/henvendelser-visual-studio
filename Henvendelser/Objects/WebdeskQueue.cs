﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser.Objects
{
    class WebdeskQueue : ContactQueue
    {
        public WebdeskQueue(DateTime period)
        {
            this.period = period;
        }
        public int total_amount { get; set; }
        public int answerPercentage_8030 { get; set; }
        public int answerPercentage_Total { get; set; }
        public override DateTime period {get; set;}
        public override string type { get; set; }
        public override string toString()
        {
            return type;
        }
    }
}
