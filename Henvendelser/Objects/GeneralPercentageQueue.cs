﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser.Objects
{
    class GeneralPercentageQueue : ContactQueue
    {
        public GeneralPercentageQueue(DateTime period)
        {
            this.period = period;
        }
        public int phone_answer_total {get; set;}
        public int phone_answer_percentage_8025 { get; set; }
        public int web_answer_percentage_8030 { get; set; }
        public int web_answer_percentage_total { get; set; }
        public int mail_answer_percentage { get; set; }
        public override DateTime period { get; set; }
        public override string type { get; set; }
        public override string toString()
        {
            return period.ToString();
        }
    }
}
