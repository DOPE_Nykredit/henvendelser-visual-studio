﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser.Objects
{
    class PhonePercentQueue : ContactQueue
    {
        public PhonePercentQueue(String type, DateTime period)
        {
            this.period = period;
            this.type = type;
        }
        public int answerPercentage { get; set; }
        public override DateTime period { get; set; }
        public override string type { get; set; }
        public override string toString()
        {
            return type;
        }
    }
}
