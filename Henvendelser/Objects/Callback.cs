﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Henvendelser.Objects
{
    class Callback : ContactQueue
    {
        public int completedCallbacks{get; set;}
        public int completed_within_timeframe{get; set;}
        public int answerPercentage { get; set; }
        public override String type {get; set;}
        public override DateTime period { get; set; }
        public Callback(String type,DateTime period)
        {
            this.type = type;
            this.period = period;
        }
        public override String toString()
        {
            return type;
        }
    }
}
