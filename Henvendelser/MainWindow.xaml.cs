﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Henvendelser.Logic.Statistics;
using Henvendelser.Objects;
using System.Windows.Controls.DataVisualization.Charting;
using System.Threading;
using System.ComponentModel;


namespace Henvendelser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ChartDataCreator cdc = new ChartDataCreator();
        private Operator op = new Operator();
        private Dictionary<String, List<ContactQueue>> data;
        public MainWindow()
        {

            InitializeComponent();
            LoadColumnChartData();
            datepicker.DefaultValue = DateTime.Now;
            DateTime start = new DateTime(2013, 02, 15, 10, 0, 0);
            DateTime end = new DateTime(2013, 02, 20, 10, 0, 0);
            Callback cb = new Callback("Callback",start);
            ThreadCollecting(start,end);

        }

        private void LoadColumnChartData()
        {
            DateTime dt = new DateTime(2013-03-01);
            ContactQueue cq = new Callback("Marc", dt);
            ((LineSeries)Henvendelser.Series[0]).ItemsSource =        
            new KeyValuePair<int, int>[]{
            new KeyValuePair<int,int>(1, 12),
            new KeyValuePair<int,int>(1, 25),
            new KeyValuePair<int,int>(3, 5),
            new KeyValuePair<int,int>(4, 6),
            new KeyValuePair<int,int>(5, 10),
            new KeyValuePair<int,int>(6, 4),
            new KeyValuePair<int,int>(7, 40),
            new KeyValuePair<int,int>(8, 12),
            new KeyValuePair<int,int>(9, 25),
            new KeyValuePair<int,int>(10, 5),
            new KeyValuePair<int,int>(11, 6),
            new KeyValuePair<int,int>(12, 10),
            new KeyValuePair<int,int>(13, 4),
            new KeyValuePair<int,int>(14, 8),
            new KeyValuePair<int,int>(15, 9),
            new KeyValuePair<int,int>(16, 50),
            new KeyValuePair<int,int>(17, 40) };

            ((LineSeries)Henvendelser.Series[1]).ItemsSource =
               new KeyValuePair<int, int>[]{
                new KeyValuePair<int, int>(1,5),
                new KeyValuePair<int, int>(2,10),
                new KeyValuePair<int,int>(3, 7),
                new KeyValuePair<int,int>(4, 8),
                new KeyValuePair<int,int>(5, 15),
                new KeyValuePair<int,int>(6, 13),
                new KeyValuePair<int,int>(7, 4),
                new KeyValuePair<int,int>(8, 1),
                new KeyValuePair<int,int>(9, 5),
                new KeyValuePair<int,int>(10, 25),
                new KeyValuePair<int,int>(11, 16),
                new KeyValuePair<int,int>(12, 40),
                new KeyValuePair<int,int>(13, 24),
                new KeyValuePair<int,int>(14, 18),
                new KeyValuePair<int,int>(15, 19),
                new KeyValuePair<int,int>(16, 50),
                new KeyValuePair<int,int>(17, 40) };

         }


        /*
         * 
         * Start of Collector methods
         *
         */       
        private void btn_hentData_Click(object sender, RoutedEventArgs e)
        {
            cdc.dateChoice = datebox.SelectedItem.ToString();
            DateTime start = new DateTime(2013, 03, 15, 10, 0, 0);
            DateTime end = new DateTime(2013, 03, 20, 10, 0, 0);
            ThreadCollecting(start, end);
        }

        public void ThreadCollecting(DateTime start, DateTime end)
        {
            this.op.startDate = start; this.op.endDate = end;
            BackgroundWorker bw = new BackgroundWorker();
            bw.WorkerReportsProgress = false;
            bw.WorkerSupportsCancellation = false;
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
            if (bw.IsBusy != true)
            {
                bw.RunWorkerAsync();
            }
        }
        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            op.Start();
        
        }
        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.data = op.getUpdatedDateList();
            this.tbProgress.Text = "Done!";
            MessageBox.Show(""+data.Count);
            /// Update Graf.
        }
        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progress.Value = e.ProgressPercentage;
            this.tbProgress.Text = (e.ProgressPercentage.ToString() + "%");
        }

        /*
         * End of collect methods
         */ 

        /*
         * Update datelabel events
         * 
         */
        private void dateTimePicker1_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

            DateTime date = (DateTime)datepicker.Value;
            datepicker.Text = date.Date.ToString();
            UpdateDateLabels(date);
        }

        private void UpdateDateLabels(DateTime date) 
        {
            dstart.Content = date.Date.ToShortDateString();
    
            switch (datebox.SelectedIndex) 
            {
                case 0:
                    date.AddDays(1);
                    break;
                case 1:
                    date = date.AddDays(7);
                    break;
                case 2:
                    date = date.AddMonths(1);
                    break;
                case 3:
                    date = date.AddYears(1);
                    break;
                default:
                    break; 
            }
            MessageBox.Show(datebox.SelectedValue.ToString());
            dend.Content = date.Date.ToShortDateString();
        
        }
        private void updateDateInformation() 
        {
            DateTime currentDate = (DateTime)datepicker.Value;
            String dateString = "";
            switch (datebox.SelectedIndex) 
            {
                case 0:
                case 1:
                    dateString =""+ weekNumber(currentDate);
                    break;
                case 2:
                    dateString = currentDate.ToString("MMMM");
                    break;
                case 3:
                    dateString = currentDate.ToString("YYYY");
                    break;
                default:
                    break;
            }
            txt_datevalue.Text = dateString;
        }


        //Date event methods
        private void datebox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            /*
            if (datebox.SelectedValue.ToString() != null) 
            {
                lbl_dateinfo.Content = datebox.SelectedValue.ToString();
                updateDateInformation();     
            }
             */
        }


        /*
         * End of date events
         *
         */ 

        /*
         * Update main content methods (treeview event items)
         */ 
        private void Callbacks_Selected(object sender, RoutedEventArgs e)
        {
            chartContent.Children.Clear();
        }

        private void External_Selected(object sender, RoutedEventArgs e)
        {
            //    chartContent.Children.Add(cdc.getChart("none"));
        }

        private void dataGrid1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

       

        /*
         * End of update main content (treeview event items)
         */ 


       /*
        *  Date Convert Functions
        */

        private int weekNumber(DateTime fromDate)
        {
            // Get jan 1st of the year
            DateTime startOfYear = fromDate.AddDays(-fromDate.Day + 1).AddMonths(-fromDate.Month + 1);
            // Get dec 31st of the year
            DateTime endOfYear = startOfYear.AddYears(1).AddDays(-1);
            // ISO 8601 weeks start with Monday 
            // The first week of a year includes the first Thursday 
            // DayOfWeek returns 0 for sunday up to 6 for saterday
            int[] iso8601Correction = { 6, 7, 8, 9, 10, 4, 5 };
            int nds = fromDate.Subtract(startOfYear).Days + iso8601Correction[(int)startOfYear.DayOfWeek];
            int wk = nds / 7;
            switch (wk)
            {
                case 0:
                    // Return weeknumber of dec 31st of the previous year
                    return weekNumber(startOfYear.AddDays(-1));
                case 53:
                    // If dec 31st falls before thursday it is week 01 of next year
                    if (endOfYear.DayOfWeek < DayOfWeek.Thursday)
                        return 1;
                    else
                        return wk;
                default: return wk;
            }
        }

        private void txt_datevalue_TextChanged(object sender, TextChangedEventArgs e)
        {
            DateTime currentlySelectedDate = (DateTime) datepicker.Value;
            switch (datebox.SelectedIndex) 
            {
                case 0:
                    if (System.DateTime.DaysInMonth(currentlySelectedDate.Year, currentlySelectedDate.Month) <= int.Parse(txt_datevalue.Text))
                    {
                        currentlySelectedDate = new DateTime(currentlySelectedDate.Year, currentlySelectedDate.Month, int.Parse(txt_datevalue.Text), 10, 0, 0);
                    }
                    else {
                        
                        MessageBox.Show("Måneden "+currentlySelectedDate.ToString("MMMM") +"Har ikke "+txt_datevalue.Text+" Dage");
                    }
                 break;
            
            }
            datepicker.Value = (DateTime)currentlySelectedDate;
        }
    }
}
