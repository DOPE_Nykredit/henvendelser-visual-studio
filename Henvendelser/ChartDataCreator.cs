﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls.DataVisualization.Charting;
using Henvendelser.Objects;
using System.Windows.Data;
namespace Henvendelser
{
    class ChartDataCreator
    {
        private Dictionary<String, List<ContactQueue>> dataList = new Dictionary<string,List<ContactQueue>>();
        private Dictionary<String, Chart> charts = new Dictionary<string, Chart>();
        public String dateChoice { get; set; }
        public ChartDataCreator() {
        
        }

        public void CreateAllCharts(Dictionary<String, List<ContactQueue>> data) 
        {
            this.dataList = data;
            charts.Clear();
            foreach (String name in dataList.Keys)
            {
                addChartToList(name);

            }       
        }
       
        private void addChartToList(String choice) {
            Chart c = new Chart();
            switch (choice)
            { 
            
                case "Externe Henvendelser":
                    List<Series> seriesData = getPhonePercentSeries(dataList["Externe"]);
                    foreach (Series ls in getPhonePercentSeries(dataList["Externe"]))
                    {
                        c.Series.Add(ls);
                    }
                    break;
                case "Interne Henvendelser":

                case "Email Antal":

                case "Email håndtering":

                case "Besvarede henvendelser":
            
                case "Telefonisk besvarelsesprocent":

                case "Svarprocenter på kanaler":

                case "Callbacks":

                default: break;
            }

            charts.Add(choice, c);
/*
            LineSeries lss = new LineSeries();
          /*  ls.IndependentValueBinding="{Binding Path=Key}";
            ls.DependentValueBinding="{Binding Path=Value}";
            
  
           lss.ItemsSource =

            new KeyValuePair<int, int>[]{
            new KeyValuePair<int,int>(1, 12),
            new KeyValuePair<int,int>(2, 25),
            new KeyValuePair<int,int>(3, 5),
            new KeyValuePair<int,int>(4, 6),
            new KeyValuePair<int,int>(5, 10),
            new KeyValuePair<int,int>(6, 4),
            new KeyValuePair<int,int>(7, 40),
            new KeyValuePair<int,int>(8, 12),
            new KeyValuePair<int,int>(9, 25),
            new KeyValuePair<int,int>(10, 5),
            new KeyValuePair<int,int>(11, 6),
            new KeyValuePair<int,int>(12, 10),
            new KeyValuePair<int,int>(13, 4),
            new KeyValuePair<int,int>(14, 8),
            new KeyValuePair<int,int>(15, 9),
            new KeyValuePair<int,int>(16, 50),
            new KeyValuePair<int,int>(17, 40) };
            c.Series.Add(lss);
  */    
        
        }

        public List<Series> CreateLineSeries(List<ContactQueue> data) 
        {
            List<Series> dataSeriesList = new List<Series>();

          /*  foreach (ContactQueue emailDataQueue in data)
            {
                if (contactqueue.period == emailDataQueue.period)
                {
                    ((GeneralPercentageQueue)contactqueue).mail_answer_percentage = ((GeneralPercentageQueue)emailDataQueue).mail_answer_percentage;
                }
            }
            */
            return dataSeriesList;
        }

        public List<Series> getExternalSeries(List<HenvendelserQueue> henvendelser, List<WebdeskQueue> web)
        {
            /**
             * To do
             * ......
             */ 
            return null;
            
        }
        public List<Series> getPhonePercentSeries(List<ContactQueue> data) 
        {
            List<Series> result = new List<Series>();
            List<KeyValuePair<String, int>> tempList = new List<KeyValuePair<string, int>>();
            Dictionary<String,  List<KeyValuePair<String, int>>> finalList = new Dictionary<String,  List<KeyValuePair<String, int>>>();
            String queueName = data[0].type;
          
            foreach (PhonePercentQueue ppq in data) 
            
            {
                if (ppq.type == queueName)
                {
                    tempList.Add(new KeyValuePair<string, int>(ppq.ReWritePeriod(dateChoice), ppq.answerPercentage));
                }
                else
                {
                    finalList.Add(queueName, tempList);
                    tempList.Clear();
                    queueName = ppq.type;
                    tempList.Add(new KeyValuePair<string, int>(ppq.ReWritePeriod(dateChoice), ppq.answerPercentage));
                }
            }
            foreach (KeyValuePair<string, List<KeyValuePair<String, int>>> entry in finalList)
            {
                LineSeries ls = new LineSeries();
                // For new binding you provide it the string path to your property.
                Binding bindInd = new Binding("Key");
                Binding bindDep = new Binding("Value");
                // then you can set the properties of your binding like so
                ls.ItemsSource = entry.Value;
                /*  bindInd.Source = 
                bindDep.Source = <Your Source>;
                */
                bindDep.Mode = BindingMode.OneWay;
                bindInd.Mode = BindingMode.OneWay;
                ls.IndependentValueBinding = bindInd;
                ls.DependentValueBinding =bindDep;
                result.Add(ls);
            }

            return result;
        }
    }
}
